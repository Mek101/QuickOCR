use std::{
    env,
    fs::File,
    io::{BufRead, BufReader, BufWriter, Result, Write},
    path::{Path, PathBuf},
};

const LANG_TS_DATA_PATH: &str = "data/lang/tesseract.txt";
const LANG_ISO369_TESSERACT_TABLE_PATH: &str = "data/lang/iso369-1_tesseract_table.txt";
const LANG_RS_FILE: &str = "lang_gen.rs";

struct TesseractRow {
    tag: String,
    name: String,
    //    display_name: String,
}

struct Iso369ToTesseractTableRow {
    iso369_tag: String,
    tesseract_tag: String,
}

struct Iso369TesseractNameRow {
    iso369_tag: String,
    tesseract_name: String,
}

fn get_reader(path: &str) -> BufReader<File> {
    BufReader::new(
        File::open(Path::new(path)).unwrap_or_else(|_| panic!("Unable to open '{}'.", path)),
    )
}

fn get_rows<F, R>(reader: BufReader<File>, map_func: F) -> Vec<R>
where
    F: Fn(&[&str]) -> Option<R>,
{
    reader
        .lines()
        .filter_map(|l| {
            let line = l.expect("Bad data stream");

            let row_contents: Vec<&str> = line.split('\t').collect();
            if row_contents[0].starts_with('#') {
                None
            } else {
                map_func(&row_contents)
            }
        })
        .collect()
}

fn get_ts_lang_data() -> Vec<TesseractRow> {
    get_rows(get_reader(LANG_TS_DATA_PATH), |row| {
        //        let display_name = row[2..].join(" ");
        Some(TesseractRow {
            tag: row[0].into(),
            name: row[1].into(),
            //            display_name
        })
    })
}

fn get_iso369_ts_table_data() -> Vec<Iso369ToTesseractTableRow> {
    get_rows(get_reader(LANG_ISO369_TESSERACT_TABLE_PATH), |row| {
        if row.len() > 1 {
            Some(Iso369ToTesseractTableRow {
                iso369_tag: row[0].into(),
                tesseract_tag: row[1].into(),
            })
        } else {
            None
        }
    })
}

fn build_conversion_table(
    table: &[Iso369ToTesseractTableRow],
    ts_rows: &[TesseractRow],
) -> Vec<Iso369TesseractNameRow> {
    table
        .iter()
        .filter_map(|table_row| {
            ts_rows
                .iter()
                .find(|ts_row| ts_row.tag == table_row.tesseract_tag)
                .map(|r| Iso369TesseractNameRow {
                    iso369_tag: table_row.iso369_tag.clone(),
                    tesseract_name: r.name.clone(),
                })
        })
        .collect()
}

fn write_enum(values: &[TesseractRow], out: &mut BufWriter<File>) -> Result<()> {
    writeln!(out, "#[derive(PartialEq, Eq, Hash, Clone, Copy)]")?;
    writeln!(out, "pub enum Language {{")?;
    for t in values.iter() {
        writeln!(out, "    {},", t.name)?;
    }
    writeln!(out, "}}")?;
    Ok(())
}

fn write_from_tesseract_tag(values: &[TesseractRow], out: &mut BufWriter<File>) -> Result<()> {
    writeln!(
        out,
        "fn from_tesseract_tag(tag: &str) -> Option<Language> {{"
    )?;
    writeln!(out, "    match tag {{")?;
    for t in values.iter() {
        writeln!(
            out,
            "        \"{0}\" => Some(Language::{1}),",
            t.tag, t.name
        )?;
    }
    writeln!(out, "        &_ => None")?;
    writeln!(out, "    }}")?;
    writeln!(out, "}}")?;
    Ok(())
}

fn write_to_tesseract_tag(values: &[TesseractRow], out: &mut BufWriter<File>) -> Result<()> {
    writeln!(out, "fn get_tesseract_tag(lang: &Language) -> &str {{")?;
    writeln!(out, "    match lang {{")?;
    for t in values.iter() {
        writeln!(out, "        Language::{0} => \"{1}\",", t.name, t.tag)?;
    }
    writeln!(out, "    }}")?;
    writeln!(out, "}}")?;
    Ok(())
}

fn write_to_iso369tag(values: &[Iso369TesseractNameRow], out: &mut BufWriter<File>) -> Result<()> {
    writeln!(out, "fn get_iso369_tag(lang: &Language) -> Option<&str> {{")?;
    writeln!(out, "    match lang {{")?;
    for t in values.iter() {
        writeln!(
            out,
            "        Language::{0} => Some(\"{1}\"),",
            t.tesseract_name, t.iso369_tag
        )?;
    }
    writeln!(out, "        &_ => None")?;
    writeln!(out, "    }}")?;
    writeln!(out, "}}")?;
    Ok(())
}

fn write_from_iso369_tag(
    values: &[Iso369TesseractNameRow],
    out: &mut BufWriter<File>,
) -> Result<()> {
    writeln!(out, "fn from_iso369_tag(tag: &str) -> Option<Language> {{")?;
    writeln!(out, "    match tag {{")?;
    for t in values {
        writeln!(
            out,
            "        \"{0}\" => Some(Language::{1}),",
            &t.iso369_tag, &t.tesseract_name
        )?;
    }
    writeln!(out, "        &_ => None")?;
    writeln!(out, "    }}")?;
    writeln!(out, "}}")?;
    Ok(())
}

fn create_lang_rs(
    ts_rows: &[TesseractRow],
    iso369_ts_table: &[Iso369TesseractNameRow],
) -> Result<()> {
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let lang_fd = File::create(PathBuf::from(&out_dir).join(LANG_RS_FILE))
        .expect("Unable to create lang.rs file");

    let mut out = BufWriter::new(lang_fd);

    write_enum(ts_rows, &mut out)?;
    writeln!(out)?;
    write_from_tesseract_tag(ts_rows, &mut out)?;
    writeln!(out)?;
    write_to_tesseract_tag(ts_rows, &mut out)?;
    writeln!(out)?;
    write_from_iso369_tag(iso369_ts_table, &mut out)?;
    writeln!(out)?;
    write_to_iso369tag(iso369_ts_table, &mut out)?;
    writeln!(out)?;
    Ok(())
}

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed={}", LANG_TS_DATA_PATH);

    let ts_rows = get_ts_lang_data();
    let iso369_ts_table = build_conversion_table(&get_iso369_ts_table_data(), &ts_rows);

    create_lang_rs(&ts_rows, &iso369_ts_table).expect("lang.rs write error.");
}
