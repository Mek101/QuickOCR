use adw::{prelude::*, subclass::prelude::*};
use anyhow::{self, Result};
use gtk::{
    gdk_pixbuf::Pixbuf,
    gio::{self, File},
    glib::{self, MainContext, PRIORITY_DEFAULT},
    subclass::prelude::GtkApplicationImpl,
    FileChooserAction, FileChooserNative, FileFilter, ResponseType,
};
use once_cell::unsync::OnceCell;

use crate::config::{TESSERACT_DATA_DIR, VERSION};
use crate::error::AppError;
use crate::ocr::{ImageData, Language, OcrWorkPool};
use crate::window_state::WindowState;
use crate::QuickocrAppWindow;

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct QuickocrApplication {
        ocr_work_pool: OnceCell<OcrWorkPool>,
        image_file_filter: OnceCell<FileFilter>,
        all_file_filter: OnceCell<FileFilter>,
    }

    impl QuickocrApplication {
        pub fn get_pool(&self) -> &OcrWorkPool {
            self.ocr_work_pool.get_or_init(|| {
                match OcrWorkPool::new(TESSERACT_DATA_DIR) {
                    Ok(pool) => {
                        log::info!("OCR pork pool initialized");
                        pool
                    }
                    Err(err) => {
                        log::error!("Unable to initialize Ocr work pool: {}", err);
                        panic!("Unable to initialize Ocr work pool: {}", err);
                    }
                }
            })
        }

        pub fn get_image_file_filter(&self) -> &FileFilter {
            self.image_file_filter.get_or_init(|| {
                let filter = FileFilter::new();
                for format in Pixbuf::formats() {
                    for extension in format.extensions() {
                        filter.add_suffix(extension.as_str());
                    }

                    for mime in format.mime_types() {
                        filter.add_mime_type(mime.as_str());
                    }
                }

                filter.set_name(Some(&gettextrs::gettext("Image files")));
                filter
            })
        }

        pub fn get_all_file_filter(&self) -> &FileFilter {
            self.all_file_filter.get_or_init(|| {
                let filter = FileFilter::new();
                filter.set_name(Some(&gettextrs::gettext("All files")));
                filter.add_pattern("*");
                filter
            })
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QuickocrApplication {
        const NAME: &'static str = "QuickocrApplication";
        type Type = super::QuickocrApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for QuickocrApplication {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
        }
    }

    impl ApplicationImpl for QuickocrApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self, application: &Self::Type) {
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = QuickocrAppWindow::new(application);
                window.upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();
        }
    }

    impl GtkApplicationImpl for QuickocrApplication {}
    impl AdwApplicationImpl for QuickocrApplication {}
}

glib::wrapper! {
    pub struct QuickocrApplication(ObjectSubclass<imp::QuickocrApplication>)
        @extends gio::Application, gtk::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

// UI functions
impl QuickocrApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::new(&[("application-id", &application_id), ("flags", flags)])
            .expect("Failed to create QuickocrApplication")
    }

    fn setup_gactions(&self) {
        let quit_action = gio::SimpleAction::new("quit", None);
        quit_action.connect_activate(glib::clone!(@weak self as app => move |_, _| {
            app.quit();
        }));
        self.add_action(&quit_action);

        let about_action = gio::SimpleAction::new("about", None);
        about_action.connect_activate(glib::clone!(@weak self as app => move |_, _| {
            app.show_about();
        }));
        self.add_action(&about_action);

        // app.open-image
        let open_image_action = gio::SimpleAction::new("open-image", None);
        open_image_action.connect_activate(glib::clone!(@weak self as app => move |_, _| {
            app.open_image_filechooser();
        }));
        self.set_accels_for_action("app.open-image", &["<primary>o"]);
        self.add_action(&open_image_action);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let dialog = gtk::builders::AboutDialogBuilder::new()
            .transient_for(&window)
            .modal(true)
            .program_name("QuickOCR")
            .version(VERSION)
            .authors(vec!["Mek101".into()])
            .build();

        dialog.present();
    }

    fn open_image_filechooser(&self) {
        if let Option::<QuickocrAppWindow>::Some(window) =
            self.active_window().map(|w| w.downcast().unwrap())
        {
            if window.get_state() == WindowState::MainViewState {
                let dialog = FileChooserNative::builder()
                    .transient_for(&self.active_window().unwrap())
                    .modal(true)
                    .select_multiple(false)
                    // https://github.com/gtk-rs/gtk-rs-core/issues/556
                    //.filter(self.imp().get_image_file_filter())
                    //.filter(self.imp().get_all_file_filter())
                    .action(FileChooserAction::Open)
                    .build();

                dialog.add_filter(self.imp().get_image_file_filter());

                dialog.connect_response(
                    glib::clone!(@strong dialog, @weak window, @weak self as app => move |_, response| {
                        if response == ResponseType::Accept {
                            if let Some(file) = dialog.file() {
                                MainContext::default().spawn_local(glib::clone!(@strong file => async move {
                                    if let Err(err) = app.run_ocr(file, app.get_default_language(), window).await {
                                        log::warn!("Error running ocr: {}", err);
                                    }
                                }));
                            } else {
                                log::error!("Reponse accepted, but no file selected");
                            }
                        }
                    })
                );

                dialog.show();
            }
        }
    }
}

// Logic functions
impl QuickocrApplication {
    fn get_default_language(&self) -> Language {
        // TODO: memorize and load the last used language form gscheme

        let default_pango_lang = gtk::default_language();

        // Cut until gnome runtime 42. Compiling Gtk directly doesn't include a pango version >= 1_46.
        // Try from the user's preferred languages first.
        //for p_lang in default_pango_lang.preferred() {
        //    if let Some(lang) = Language::from_rfc3066_tag(p_lang.to_string().as_str()) {
        //        return lang;
        //    }
        //}

        // Try from the user's default language.
        if let Some(lang) = Language::from_rfc3066_tag(default_pango_lang.to_string().as_str()) {
            return lang;
        }

        // Use the enum's default value
        Language::default()
    }

    async fn load_picture(file: File) -> Result<Pixbuf> {
        let istream = file.read_future(PRIORITY_DEFAULT).await?;

        let mut pix_buf = Pixbuf::from_stream_future(&istream).await?;
        if let Some(rotated) = pix_buf.apply_embedded_orientation() {
            pix_buf = rotated;
        }

        Ok(pix_buf)
    }

    async fn run_ocr(
        &self,
        file: File,
        language: Language,
        window: QuickocrAppWindow,
    ) -> Result<()> {
        let path = file.path().ok_or(AppError::ImageWithoutPath)?;
        let _ = window.try_set_result_view()?;

        let pix_buf = Self::load_picture(file).await?;
        let _ = window.try_set_preview_picture(&pix_buf)?;

        let img_data = ImageData::from_path(&path)?;
        let pool = self.imp().get_pool();

        match pool.process(img_data, language).await {
            Ok(text) => window.try_set_text_result(&text)?,
            Err(err) => return Err(err),
        }
        Ok(())
    }
}
