pub const VERSION: &str = "0.1.0";
pub const GETTEXT_PACKAGE: &str = "quickocr";
pub const LOCALEDIR: &str = "/app/share/locale";
pub const PKGDATADIR: &str = "/app/share/quickocr";
pub const APP_ID: &str = "page.codeberg.mek101.QuickOCR";
pub const TESSERACT_DATA_ENV: &str = "TESSDATA_PREFIX";
pub const TESSERACT_DATA_DIR: &str = "/app/share/tessdata";
