use thiserror::Error;

#[derive(Error, Debug)]
pub enum AppError {
    #[error("The image doesn't have a path")]
    ImageWithoutPath,
}
