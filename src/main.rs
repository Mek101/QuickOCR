mod application;
mod config;
mod error;
mod ocr;
mod window;
mod window_state;

use std::env;

use adw::prelude::*;
use gtk::{
    gio,
    glib::{GlibLogger, GlibLoggerDomain, GlibLoggerFormat},
};

use crate::application::QuickocrApplication;
use crate::config::{
    APP_ID, GETTEXT_PACKAGE, LOCALEDIR, PKGDATADIR, TESSERACT_DATA_DIR, TESSERACT_DATA_ENV,
};
use crate::window::QuickocrAppWindow;

fn main() {
    static GLIB_LOGGER: GlibLogger =
        GlibLogger::new(GlibLoggerFormat::Plain, GlibLoggerDomain::CrateTarget);

    log::set_logger(&GLIB_LOGGER)
        .unwrap_or_else(|err| panic!("Error while setting up logger: {}", err));
    log::set_max_level(log::LevelFilter::Debug);

    // Set up gettext translations
    if let Err(err) = gettextrs::bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR) {
        log::warn!("Error in bindtextdomain(): {}", err);
    }
    if let Err(err) = gettextrs::bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8") {
        log::warn!("Error in bind_textdomain_codeset(): {}", err);
    }
    if let Err(err) = gettextrs::textdomain(GETTEXT_PACKAGE) {
        log::warn!("Error in textdomain(): {}", err);
    }

    // Load resources
    let resources = gio::Resource::load(PKGDATADIR.to_owned() + "/quickocr.gresource")
        .unwrap_or_else(|err| panic!("Could not load resources: {}", err));
    gio::resources_register(&resources);

    if let Ok(path) = env::var(TESSERACT_DATA_ENV) {
        log::warn!(
            "{} environment variable is set as {}. QuickOCR will ignore it in favour of hardcoded {}.",
            TESSERACT_DATA_ENV,
            path,
            TESSERACT_DATA_DIR
        );
    }

    // Create a new AdwApplication. The application manages our main loop,
    // application windows, integration with the window manager/compositor, and
    // desktop features such as file opening and single-instance applications.
    let app = QuickocrApplication::new(APP_ID, &gio::ApplicationFlags::empty());

    // Run the application. This function will block until the application
    // exits. Upon return, we have our exit code to return to the shell. (This
    // is the code you see when you do `echo $?` after running a command in a
    // terminal.
    std::process::exit(app.run());
}
