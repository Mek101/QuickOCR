use std::cell::RefCell;

use cached::{Cached, SizedCache};
use leptess::LepTess;

use crate::ocr::Language;

pub struct CachedContainerBuilder {
    tessdata_dir: String,
}

impl CachedContainerBuilder {
    pub fn new(tesseract_data_dir: &str) -> Self {
        Self {
            tessdata_dir: tesseract_data_dir.into(),
        }
    }

    pub fn build(&self) -> CachedContainer {
        CachedContainer::new(&self.tessdata_dir)
    }
}

pub struct CachedContainer {
    tessdir: String,
    cache: RefCell<SizedCache<Language, LepTess>>,
}

impl CachedContainer {
    fn new(tesseract_data_dir: &str) -> Self {
        Self {
            tessdir: tesseract_data_dir.into(),
            cache: RefCell::new(SizedCache::with_size(1)),
        }
    }

    pub fn with<F, R>(&self, lang: Language, func: F) -> R
    where
        F: FnOnce(&mut LepTess) -> R,
    {
        let mut cache = self.cache.borrow_mut();
        let mut value =
            cache.cache_get_or_set_with(lang, || {
                LepTess::new(Some(&self.tessdir), lang.as_tesseract_tag()).unwrap()
            });

        func(&mut value)
    }
}
