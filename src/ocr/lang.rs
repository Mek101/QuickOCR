// Definitions and base functions must be generated at build time.
include!(concat!(env!("OUT_DIR"), "/lang_gen.rs"));

impl Language {
    pub fn from_tesseract_tag(tag: &str) -> Option<Language> {
        // TODO: handle languages with multiple scripts.
        from_tesseract_tag(tag)
    }

    pub fn as_tesseract_tag(&self) -> &str {
        get_tesseract_tag(self)
    }

    /// Returns a language as RFC-3066 tag.
    pub fn as_iso369_tag(&self) -> Option<&str> {
        get_iso369_tag(self)
    }

    pub fn from_rfc3066_tag(tag: &str) -> Option<Self> {
        // First subtag is language tag.
        let lang_subtag = tag.split('_').next()?;

        // By rfc 3066, 2-letters subtags are iso 639-1 tags.
        if lang_subtag.len() == 2 {
            return from_iso369_tag(tag);
        }

        // By rfc 3066, 3-letters subtags are iso 639-2 tags.
        if lang_subtag.len() == 3 {
            // Tesseract strings are a superset of iso 639-2.
            return from_tesseract_tag(lang_subtag);
        }
        None
    }
}

impl Default for Language {
    fn default() -> Self {
        Language::English
    }
}
