mod cached_container;
mod lang;

use std::path::{Path, PathBuf};
use std::sync::Arc;

use anyhow::Result;
use gtk::glib::{self, Error, ThreadPool};

use crate::ocr::cached_container::CachedContainerBuilder;

pub use lang::Language;

pub struct ImageData {
    path: PathBuf,
}

impl ImageData {
    pub fn from_path(path: &Path) -> Result<Self> {
        Ok(Self { path: path.into() })
    }
}

pub struct OcrWorkPool {
    pool: ThreadPool,
    cache_builder: Arc<CachedContainerBuilder>,
}

impl OcrWorkPool {
    pub fn new(tesseract_data_dir: &str) -> Result<Self, Error> {
        let pool = ThreadPool::shared(Some(glib::num_processors()))?;
        Ok(Self {
            pool,
            cache_builder: Arc::new(CachedContainerBuilder::new(tesseract_data_dir)),
        })
    }

    pub async fn process(
        &self,
        img_data: ImageData,
        lang: Language,
    ) -> Result<String> {
        let cache_builder = self.cache_builder.clone();
        self.pool
            .push_future(move || {
                let cache = cache_builder.build();

                cache.with(lang, |leptess| {
                    leptess.set_image(&img_data.path)?;
                    Ok(leptess.get_utf8_text()?)
                })
            })?
            .await
    }
}
