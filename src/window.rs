use std::cell::Cell;

use adw::{prelude::*, subclass::prelude::*, ApplicationWindow, Leaflet, NavigationDirection};
use gtk::{
    gdk_pixbuf::Pixbuf, gio, glib, subclass::prelude::*, template_callbacks, CompositeTemplate,
    DropDown, Picture, TextView,
};

use crate::window_state::{StateError, WindowState};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/page/codeberg/mek101/QuickOCR/gtk/window.ui")]
    pub struct QuickocrAppWindow {
        // Template widgets
        #[template_child]
        pub(super) leaflet: TemplateChild<Leaflet>,
        #[template_child]
        pub(super) picture_preview: TemplateChild<Picture>,
        #[template_child]
        pub(super) text_result: TemplateChild<TextView>,
        #[template_child]
        pub(super) main_language_drop_down: TemplateChild<DropDown>,
        #[template_child]
        pub(super) result_language_drop_down: TemplateChild<DropDown>,
        pub(super) state: Cell<WindowState>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QuickocrAppWindow {
        const NAME: &'static str = "QuickocrAppWindow";
        type Type = super::QuickocrAppWindow;
        type ParentType = ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::Type::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for QuickocrAppWindow {}
    impl WidgetImpl for QuickocrAppWindow {}
    impl WindowImpl for QuickocrAppWindow {}
    impl ApplicationWindowImpl for QuickocrAppWindow {}
    impl AdwApplicationWindowImpl for QuickocrAppWindow {}
}

glib::wrapper! {
    pub struct QuickocrAppWindow(ObjectSubclass<imp::QuickocrAppWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl QuickocrAppWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)])
            .expect("Failed to create QuickocrAppWindow")
    }

    pub fn get_state(&self) -> WindowState {
        self.imp().state.get()
    }

    pub fn try_set_result_view(&self) -> Result<(), StateError> {
        let imp = self.imp();
        let state = imp.state.get();
        if state != WindowState::ResultViewState {
            imp.state.set(WindowState::ResultViewState);

            // Reset picture preview.
            imp.picture_preview.set_pixbuf(None);
            // Navigate to the result view.
            imp.leaflet.navigate(NavigationDirection::Forward);
            return Ok(());
        }
        Err(StateError::WindowAnotherState(state))
    }

    pub fn try_set_main_view(&self) -> Result<(), StateError> {
        let imp = self.imp();
        let state = imp.state.get();
        if imp.state.get() != WindowState::MainViewState {
            imp.state.set(WindowState::MainViewState);

            // Reset picture preview.
            imp.picture_preview.set_pixbuf(None);
            // Reset text result
            imp.text_result.buffer().set_text("");
            // Navigate to the main view.
            imp.leaflet.navigate(NavigationDirection::Back);
            return Ok(());
        }
        Err(StateError::WindowAnotherState(state))
    }

    pub fn try_set_preview_picture(&self, pix_buf: &Pixbuf) -> Result<(), StateError> {
        let imp = self.imp();
        let state = imp.state.get();
        if state == WindowState::ResultViewState {
            imp.picture_preview.set_pixbuf(Some(pix_buf));
            return Ok(());
        }
        Err(StateError::WindowAnotherState(state))
    }

    pub fn try_set_text_result(&self, text: &str) -> Result<(), StateError> {
        let imp = self.imp();
        let state = imp.state.get();
        if state == WindowState::ResultViewState {
            imp.text_result.buffer().set_text(text);
            return Ok(());
        }
        Err(StateError::WindowAnotherState(state))
    }
}

// Signal handling
#[template_callbacks]
impl QuickocrAppWindow {
    #[template_callback]
    pub fn on_back_button_clicked(&self) {
        let _ = self.try_set_main_view();
    }
}
