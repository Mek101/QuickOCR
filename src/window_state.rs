//use gtk::glib::WeakRef;
use thiserror::Error;

//use crate::window::QuickocrAppWindow;

#[derive(Debug, Error)]
pub enum StateError {
    #[error("The window has been closed. Can't upgrade the state")]
    WindowDisposed,
    #[error("The window has another state. Can't upgrade the state")]
    WindowAnotherState(WindowState),
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum WindowState {
    MainViewState,
    ResultViewState,
}

impl Default for WindowState {
    fn default() -> Self {
        WindowState::MainViewState
    }
}
/*
fn get_if_state(window: WeakRef<QuickocrAppWindow>, internal_state: WindowState) -> Result<QuickocrAppWindow, StateError> {
    let win = window.upgrade().ok_or(StateError::WindowDisposed)?;
    let win_state = win.get_state();
    if win_state == internal_state {
        Ok(win)
    } else {
        Err(StateError::WindowAnotherState(win_state))
    }
}

fn win_to_ref(window: QuickocrAppWindow) -> WeakRef<QuickocrAppWindow> {
    let win_ref = WeakRef::new();
    win_ref.set(Some(&window));
    win_ref
}

pub fn state_from_window(window: QuickocrAppWindow) -> Result<OpenFileState, StateError> {
    let win_state = window.get_state();
    if win_state == WindowState::OpenFile {
        Ok(OpenFileState(win_to_ref(window)))
    } else {
        Err(StateError::WindowAnotherState(win_state))
    }
}

pub struct OpenFileState(WeakRef<QuickocrAppWindow>);

impl OpenFileState {
    pub fn upgrade_to_waiting_tesseract(self) -> Result<WaitResultState, StateError> {
        let win = get_if_state(self.0, WindowState::OpenFile)?;
        Ok(WaitResultState(win_to_ref(win)))
    }
}

pub struct WaitResultState(WeakRef<QuickocrAppWindow>);

impl WaitResultState {
    pub fn try_set_image_preview(&self) -> bool { false }

    pub fn upgrade_to_display_result(self, res: &str) -> Result<DisplayResultState, StateError> {
        let win = get_if_state(self.0, WindowState::WaitForTesseract)?;
        Ok(DisplayResultState(win_to_ref(win)))
    }

    pub fn reset(self) -> OpenFileState {
        OpenFileState(self.0)
    }
}

pub struct DisplayResultState(WeakRef<QuickocrAppWindow>);

impl DisplayResultState {
    pub fn try_set_image_preview(&self) -> bool { false }

    pub fn reset(self) -> OpenFileState {
        OpenFileState(self.0)
    }
}*/
